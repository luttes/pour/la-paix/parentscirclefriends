
.. _bassam_aramin:

==================
Bassam Aramin
==================

Bassam Aramin lives in Jericho in the West Bank. 
At the age of 17, he was incarcerated and spent 7 years in an Israeli jail. 

He went on to study history and holds an MA in Holocaust studies from 
the University of Bradford, England. He became a member of the Parents 
Circle in 2007 after losing his 10-year-old daughter Abir, who was killed 
by an Israeli border policeman in front of her school. 

Bassam devotes his time and energies to his conviction for a peaceful, 
nonviolent end to the Israeli occupation of Palestine and to Israeli-Palestinian 
reconciliation. Bassam is the former Palestinian Co-director of the 
Parents Circle – Families Forum.
