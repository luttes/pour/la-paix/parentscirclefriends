
.. _rami_elhanan:

===================
**Rami Elhanan**
===================

Rami Elhanan is a 7th generation Jerusalemite. 

He identifies himself as a Jew, an Israeli, and before everything else a 
human being. On the first day of the school year in 1997, Rami’s daughter, 
Smadar, was killed by two Palestinian suicide bombers who murdered 5 people 
that day. 

Soon after, Rami joined the Parents Circle, and speaks before Israeli, 
Palestinian and International audiences. 

Rami is the former Israeli Co-director of the Parents Circle – Families Forum.

