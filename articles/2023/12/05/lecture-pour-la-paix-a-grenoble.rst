
.. _lecture_2023_12_05:

=====================================================================================================================================
2023-12-05 **Lecture d'extraits du roman Apeirogon (de Colum McCann) pour la paix entre Israeliens et Palestinens (Grenoble)**
=====================================================================================================================================

- https://kolektiva.social/@grenobleluttes/111520940866794597
- https://parentscirclefriends.org/
- https://www.youtube.com/@ThePCFF/videos
- https://parentscirclefriends.org/voices/
- https://www.instagram.com/parentscirclefriends/
- https://linktr.ee/afpcff

#paix #dialogue #israël #palestine #LaCimade #grenoble #Together4Humanity
#parentscircle

2023-11-30 Webinar: Voices of Apeirogon, with Colum McCann and Bereaved Fathers
===================================================================================

- https://www.youtube.com/watch?v=J0ASOZ3XZA0

.. figure:: images/mac_cann_bassam_rami.png

A conversation with renowned Irish author Colum McCann. 

Colum engaged in a heartfelt dialogue with Rami Elhanan and Bassam Aramin, 
bereaved fathers from Israel and Palestine, the protagonists of Apeirogon. 

In the face of the violence shaking Israel and Palestine, join us to hear 
from Rami and Bassam, who, despite the pain of loss, still call each other 
brothers. 

McCann’s powerful 2020 novel Apeirogon weaves together tales of peace, 
loss, and reconciliation. 

The book’s exploration of humanity becomes even more relevant today.

Learn more about the Parents Circle - Families Forum at https://www.parentscirclefriends.org

.. youtube:: J0ASOZ3XZA0

**Le roman Apeirogon de Colum McCann**
===========================================

- https://fr.wikipedia.org/wiki/Apeirogon_(roman)

Apeirogon (titre original : Apeirogon) est un roman, publié en 2020, par 
l'écrivain irlandais Colum McCann. 

La même année, le roman est traduit en français par Clément Baude pour le 
compte des éditions Belfond. 

Réception francophone
----------------------------

- https://fr.wikipedia.org/wiki/Apeirogon_(roman)#R%C3%A9ception_francophone

Pour Philippe Chevilley (Les Échos) : « Construit autour des figures 
héroïques de deux combattants de la paix, l'un juif israélien, l'autre 
musulman palestinien, le nouveau roman composite de l'écrivain irlandais 
est un prodige d'écriture et une leçon d'humanisme. 

Colum McCann signe son chef-d'œuvre


parentscirclefriends Description
=======================================

- https://parentscirclefriends.org/
- https://www.youtube.com/@ThePCFF/videos
- https://parentscirclefriends.org/voices/
- https://www.instagram.com/parentscirclefriends/
- https://linktr.ee/afpcff


American Friends of the Parents Circle – Families Forum shares the human side 
of the Israeli-Palestinian conflict with the American public in order to foster 
a peace and reconciliation process.

The Parents Circle – Families Forum is a joint Israeli-Palestinian organization 
made up of more than 700 bereaved families. 

Their common bond is that they have lost a close family member to the conflict. 

But instead of choosing revenge, they have chosen a path of reconciliation.


2023-11-30 Voices of Apeirogon An Intimate Conversation with Colum McCann and Bereaved Fathers 
==================================================================================================

- https://parentscirclefriends.org/voices/
- https://www.youtube.com/watch?v=J0ASOZ3XZA0&t=1s

A conversation with renowned Irish author Colum McCann. 

Colum engaged in a  heartfelt dialogue with Rami Elhanan and Bassam Aramin, 
bereaved fathers from  Israel and Palestine, the protagonists of Apeirogon.

**In the face of the violence shaking Israel and Palestine, join us to hear from 
Rami and Bassam, who, despite the pain of loss, still call each other brothers**. 

`McCann’s powerful 2020 novel Apeirogon <https://en.wikipedia.org/wiki/Apeirogon_(novel)>`_ weaves 
together tales of peace, loss,  and reconciliation. 

The book’s exploration of humanity becomes even more relevant today.

Bassam Aramin
==================

- :ref:`bassam_aramin`

Rami Elhanan
==============

- :ref:`rami_elhanan`


Colum McCann
==============

- https://fr.wikipedia.org/wiki/Colum_McCann

Colum McCann is the author of seven novels, three collections of stories 
and two works of non-fiction. 

Born and raised in Dublin, Ireland, he has been the recipient of many 
international honours, including the U.S National Book Award, the 
International Dublin Literary Prize, a Chevalier des Arts et Lettres 
from the French government, election to the Irish arts academy, several 
European awards, the 2010 Best Foreign Novel Award in China, and an 
Oscar nomination. 

In 2017 he was elected to the American Academy of Arts. 

He is the President and co-founder of the non-profit global story 
exchange organisation, Narrative 4. His most recent novel, Apeirogon, 
became an immediate New York Times best-seller and won several major 
international awards. 

En français
----------------

À propos de Apeirogon (2020), Florence Noiville écrit « Son grand œuvre 
de la maturité.[...] 

Un hyper, un archi-roman. Explosé en exactement mille et un fragments.». 

Le mardi 5 décembre 2023 à Grenoble
========================================

.. figure:: images/lecture_pour_la_paix.png

Les Compagnies **Les Moissonneurs des Lilas** et **Le Théâtre bleu** vous 
proposent une lecture d'extraits d'Apeirogon de Column Mac Cann.

Mardi 5 décembre 2023 à 19h au salon de réception de l'hôtel de ville
de Grenoble.

*Lecture pour la paix par Jean-Luc Misson et Pascal Guin
mise en musique par Somar Al-Nasser (oud) et Sercan Genc (percussion)*.

:ref:`Rami Elhanan <rami_elhanan>` est israélien, fils d'un rescapé de la Shoah, ancien soldat de la 
guerre du Kippour;

:ref:`Bassam Aramin <bassam_aramin>` est Palestinien, a connu la déposession, la prison et les
humiliations.

Tous deux ont perdu une fille, tuée par la partie adverse. 
Smadar avait treize ans. Abir dix ans.
Passés le choc, la douleur, les souvenirs, le deuil, il y a l'envie de
sauver des vies.

**Eux qui étaient nés pour se haïr décident de raconter leur histoire et de se battre 
pour la paix**.

**La paix c'est pour ça et uniquement pour ça que nous partageons ces mots 
avec vous**.

Pour ne pas rester les bras croisés alors que cette guerre qui n'a jamais
céssé renait de plus belle.

Cette soirée est proposée avec le soutien de

- `La Cimade <https://www.facebook.com/lacimadegrenoble/>`_
- la ville de Grenoble

Les moissonneurs des Lilas
----------------------------

- https://www.lesmoissonneursdeslilas.fr/


La cimade Grenoble
-------------------------

- https://www.facebook.com/lacimadegrenoble/
